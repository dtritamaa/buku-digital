@extends('layout.master')

@section('title')
    Edit Data Buku
@endsection

@section('content')

<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="judul_buku">Judul Buku : </label>
                <input type="text" class="form-control" name="judul" value="{{$buku->judul}}" id="judul" placeholder="Masukkan Judul" required>
                @error('judul')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="kode_buku">Kode Buku : </label>
                <input type="text" class="form-control" name="kode_buku" value="{{$buku->kode_buku}}" id="kode_buku" placeholder="Masukkan Kode Buku" required>
                @error('kode_buku')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="isi">Isi</label>
                <textarea class="form-control" id="isi" name="isi" cols="30" rows="10">{{$buku->isi}}</textarea>
                @error('isi')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="tahun_terbit">Tahun Terbit : </label>
                <input type="number" class="form-control" name="tahun_terbit" value="{{$buku->tahun_terbit}}" id="tahun_terbit" placeholder="Tahun terbit" required>
                @error('tahun_terbit')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="asal_daerah">Asal Daerah : </label>
                <input type="text" class="form-control" name="asal_daerah" value="{{$buku->asal_daerah}}" id="asal_daerah" placeholder="Masukkan asal daerah" required>
                @error('asal_daerah')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="kategori">Kategori : </label>
                <select class="custom-select" name="kategori" id="kategori" >
                    <option value="{{$buku->kategori}}">{{$buku->kategori}}</option>
                    <option value="Fiksi">Fiksi</option>
                    <option value="Non-Fiksi">Non-Fiksi</option>
                </select>
                @error('kategori')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="pengarang">Pengarang : </label>
                <input type="text" class="form-control" name="pengarang" value="{{$buku->pengarang}}" id="pengarang" placeholder="Nama Pengarang">
                @error('pengarang')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="jenis">Jenis : </label>
                <select class="custom-select" name="jenis_id" id="jenis_id" >
                    <option value="">Pilih jenis buku</option>
                @foreach ($listjenis as $item)
                    @if ($item->id === $buku->jenis_id) 
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                @endforeach
                </select>
                @error('jenis_id')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="cover_buku">Cover buku : </label>
                <input type="file" class="form-control" name="cover_buku" id="cover_buku">
                @error('cover_buku')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Edit Buku</button>
        </form>
@endsection