@extends('layout.master')

@section('title')
    Data Buku
@endsection

@section('content')

<a href="/buku/create" class="btn btn-primary">Tambah Jenis Buku !</a>
        <table class="table mt-2">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Judul</th>
                <th scope="col">Kode Buku</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($listbuku  as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->kode_buku}}</td>
                        <td>
                            
                            <form action="/buku/{{$value->id}}" method="POST">
                                <a href="/buku/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/buku/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection