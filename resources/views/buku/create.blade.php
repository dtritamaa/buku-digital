@extends('layout.master')

@section('title')
    Tambah Data Buku
@endsection

@section('content')

<form action="/buku" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="judul_buku">Judul Buku : </label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul" required>
                @error('judul')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="kode_buku" class="mt-3  ">Kode Buku : </label>
                <input type="text" class="form-control" name="kode_buku" id="kode_buku" placeholder="Masukkan Kode Buku" required>
                @error('kode_buku')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="isi" class="mt-3">Isi</label>
                <textarea class="form-control" id="isi" name="isi" col="30" rows="10"></textarea>
                @error('isi')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="tahun_terbit" class="mt-3">Tahun Terbit : </label>
                <input type="number" class="form-control" name="tahun_terbit" id="tahun_terbit" placeholder="Tahun terbit" required>
                @error('tahun_terbit')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="asal_daerah" class="mt-3">Asal Daerah : </label>
                <input type="text" class="form-control" name="asal_daerah" id="asal_daerah" placeholder="Masukkan asal daerah" required>
                @error('asal_daerah')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="kategori" class="mt-3">Kategori : </label>
                <select class="custom-select" name="kategori" id="kategori" >
                    <option>Pilih kategori buku</option>
                    <option value="fiksi">Fiksi</option>
                    <option value="non-fiksi">Non-Fiksi</option>
                </select>
                @error('kategori')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="pengarang" class="mt-3">Pengarang : </label>
                <input type="text" class="form-control" name="pengarang" id="pengarang" placeholder="Nama Pengarang">
                @error('pengarang')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="jenis" class="mt-3">Jenis : </label>
                <select class="custom-select" name="jenis_id" id="jenis_id" >
                    <option>Pilih jenis buku</option>
                @foreach ($listjenis as $item)
                    <option value="{{$item->id }}">{{$item->nama}}</option>
                @endforeach
                </select>
                @error('jenis_id')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="cover_buku" class="mt-3">Cover buku : </label>
                <input type="file" class="form-control" name="cover_buku" id="cover_buku">
                @error('cover_buku')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah Buku !</button>
            <a href="/buku" class="btn btn-success pull-right">Kembali</a>
        </form>
@endsection