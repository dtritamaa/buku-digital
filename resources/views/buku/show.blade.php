@extends('layout.master')

@section('title')
    Detail Buku {{$buku->id}}
@endsection

@section('content')
    <div class="row">
        <div class="col-3">
            <img src="{{asset('coverbuku/' . $buku->cover_buku)}}" alt="">
        </div>
        <div class="col-3 mb-3">
            <h5>{{$buku->kategori}} <div class="bullet"></div> {{$buku->tahun_terbit}} </h5>
            <h3>Judul buku : {{$buku->judul}}</h3>
            <h5>Kode Buku  : {{$buku->kode_buku}}</h5>
            <h5>Pengarang  : {{$buku->pengarang}}</h5>
            <h5>Jenis Buku : {{$buku->jenis->nama}}</h5>
        </div>
    </div>
    <div class="mt-5">
        <h4>Isi cerita : </h4>
        <p>{{$buku->isi}}</p>
    </div>
    <a href="/halaman" class="btn btn-success pull-right">Kembali</a>
@endsection