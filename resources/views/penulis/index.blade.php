@extends('layout.master')

@section('title')
    Data Penulis
@endsection

@section('content')

<a href="/penulis/create" class="btn btn-primary">Tambah penulis !</a>
        <table class="table mt-2">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Asal Kota</th>
                <th scope="col">Biodata</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($listpenulis  as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->asal_kota}}</td>
                        <td>{{$value->biodata}}</td>
                        <td>
                            
                            <form action="/penulis/{{$value->id}}" method="POST">
                                <a href="/penulis/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/penulis/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection