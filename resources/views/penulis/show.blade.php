@extends('layout.master')

@section('title')
    Detail Penulis id ke {{$penulis->id}}
@endsection

@section('content')

<h5>Nama      : {{$penulis->nama}}</h5>
<h5>Umur      : {{$penulis->umur}}</h5>
<h5>Asal Kota : {{$penulis->asal_kota}}</h5>
<h5>Biodata   : {{$penulis->biodata}}</h5>


<a href="/jenis" class="btn btn-primary mt-5">Back</a>



@endsection