@extends('layout.master')

@section('title')
    Tambah Penulis Buku
@endsection

@section('content')

<form action="/penulis/{{$penulis->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama : </label>
                <input type="text" class="form-control" name="nama" value="{{$penulis->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur : </label>
                <input type="text" class="form-control" name="umur" value="{{$penulis->umur}}" id="umur" placeholder="Masukkan Umurnya">
                @error('umur')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Asal Kota : </label>
                <input type="text" class="form-control" name="asal_kota" value="{{$penulis->asal_kota}}" id="asal_kota" placeholder="Masukkan asal kota">
                @error('umur')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="biodata">Bio</label>
                <textarea class="form-control" id="biodata" name="biodata" rows="3" required>{{ $penulis->biodata }}</textarea>
                @error('biodata')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>

            <!-- <div class="form-group">
                <label for="title">Biodata : </label>
                <input type="text" class="form-control" name="biodata" id="biodata" placeholder="Masukkan Biodata">
                @error('biodata')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div> -->
            <button type="submit" class="btn btn-primary">Edit Data Penulis !</button>
        </form>

@endsection