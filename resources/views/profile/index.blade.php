@extends('layout.master')

@section('title')
    User Id {{Auth::user()->name}}
@endsection

@section('content')

    @if ($profile != null)
            <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="umur">Umur : </label>
                <input type="text" class="form-control" name="umur" value="{{$profile->umur}}" id="umur" placeholder="Masukkan Umurnya">
                @error('umur')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="biodata">Biodata : </label>
                <textarea class="form-control" id="biodata" value="{{$profile->bio}}" name="biodata" rows="5"></textarea>
                @error('biodata')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">alamat</label>
                <textarea class="form-control" id="alamat" value="{{$profile->alamat}}" name="alamat" rows="5"></textarea>
                @error('alamat')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit data</button>
            <a href="/buku" class="btn btn-success pull-right">Kembali</a>
        </form>

        @else
        <form action="/profile" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="umur">Umur : </label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umurnya">
                @error('umur')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="biodata">Bio</label>
                <textarea class="form-control" id="biodata" name="biodata" rows="5"></textarea>
                @error('biodata')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">alamat</label>
                <textarea class="form-control" id="alamat" name="alamat" rows="5"></textarea>
                @error('alamat')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah Profile</button>
            <a href="/buku" class="btn btn-success pull-right">Kembali</a>
        </form>
    @endif
@endsection