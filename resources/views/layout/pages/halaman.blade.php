@extends('layout.master')

@section('title')
    Data Buku
@endsection

@section('content')
    <div class="row">
        @foreach ($listbuku as $item)
        <div class="col-4 mt-3">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('coverbuku/'.$item->cover_buku)}}" alt="Card image cap">
                <div class="card-body">
                <h4>{{$item->judul}}</h4>
                    <form action="/buku/{{$item->id}}" method="POST">
                        <a href="/buku/{{$item->id}}" class="btn btn-info">Baca</a>

                        @auth
                        <a href="/buku/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                        @endauth
                        
                    </form>
                    
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection

           

