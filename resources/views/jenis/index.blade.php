@extends('layout.master')

@section('title')
    Data Jenis
@endsection

@section('content')

<a href="/jenis/create" class="btn btn-primary">Tambah Jenis Buku !</a>
        <table class="table mt-2">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Jenis</th>
                <th scope="col">Buku</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($listjenis  as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <ul>
                                @foreach ($value->buku as $buku)
                                <li>{{$buku->judul}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            
                            <form action="/jenis/{{$value->id}}" method="POST">
                                <a href="/jenis/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/jenis/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection