@extends('layout.master')

@section('title')
    Detail Jenis id ke {{$jenis->id}}
@endsection

@section('content')

<form action="/jenis/{{$jenis->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama : </label>
                <input type="text" class="form-control" name="nama" value="{{$jenis->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('jenis')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mt-3">Update Jenis Buku !</button>
            <a class="btn btn-success mt-3" href="/jenis">Kembali</a>
        </form>


@endsection