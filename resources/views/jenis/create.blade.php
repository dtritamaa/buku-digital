@extends('layout.master')

@section('title')
    Tambah Jenis Buku
@endsection

@section('content')

<form action="/jenis" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Jenis Buku : </label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Jenis Buku" required>
                @error('jenis')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah Jenis !</button>
            <a href="/buku" class="btn btn-success pull-right">Kembali</a>
        </form>
@endsection