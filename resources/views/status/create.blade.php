@extends('layout.master')

@section('title')
    Tambah Penulis Buku
@endsection

@section('content')

<form action="/penulis" method="POST">
            @csrf
            <label for="buku_id" class="mt-3">Buku : </label>
                <select class="custom-select" name="buku_id" id="buku_id" >
                    <option>Pilih buku</option>
                @foreach ($listbuku as $item)
                    <option value="{{$item->id }}">{{$item->judul}}</option>
                @endforeach
                </select>
                @error('buku_id')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="penulis_id" class="mt-3">Penulis : </label>
                <select class="custom-select" name="penulis_id" id="penulis_id" >
                    <option>Pilih buku</option>
                @foreach ($listpenulis as $item)
                    <option value="{{$item->id }}">{{$item->nama}}</option>
                @endforeach
                </select>
                @error('penulis_id')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <label for="status" class="mt-3">Status : </label>
                <select class="custom-select" name="status" id="status" >
                    <option>Pilih Status buku</option>
                    <option value="Complete">Complete</option>
                    <option value="Ongoing">Ongoing</option>
                </select>
                @error('status')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah Status !</button>
            <a href="/buku" class="btn btn-success pull-right">Kembali</a>
        </form>
@endsection