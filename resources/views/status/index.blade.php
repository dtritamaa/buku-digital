@extends('layout.master')

@section('title')
    Data Status
@endsection

@section('content')

<a href="/status/create" class="btn btn-primary">Tambah Status !</a>
        <table class="table mt-2">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Judul</th>
                <th scope="col">Penulis</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($liststatus  as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>
                            <ul>
                                @foreach ($value->buku as $buku)
                                <li>{{$buku->judul}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <ul>
                                @foreach ($value->penulis as $penulis)
                                <li>{{$penulis->nama}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>{{$value->status}}</td>
                        <td>
                            
                            <form action="/penulis/{{$value->id}}" method="POST">
                                <a href="/penulis/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection