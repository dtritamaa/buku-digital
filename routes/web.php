<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

Route::group(['middleware' => ['auth']], function(){
    ROute::resource('profile', 'ProfileController')->only(['index', 'update', 'store']);
    Route::resource('jenis', 'JenisController');
    Route::resource('penulis', 'PenulisController');
    

});
Route::resource('status', 'StatusController');
Route::resource('buku', 'BukuController');




Route::get('/halaman', 'HomeController@index');
Auth::routes();


