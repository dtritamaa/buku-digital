<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = ('status');
    protected $fillable = ['buku_id', 'penulis_id', 'status'];

    public $timestamps = false;


    public function buku(){
        return $this->hasMany('App\Buku');
    }

    public function penulis(){
        return $this->hasMany('App\Penulis');
    }
}
