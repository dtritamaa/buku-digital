<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['judul', 'kode_buku', 'isi', 'tahun_terbit', 'asal_daerah', 
                            'kategori', 'pengarang', 'jenis_id', 'cover_buku'];

    public function jenis(){
    return $this->belongsTo('App\Jenis');
    }

    public function status(){
    return $this->belongsTo('App\Status');
    }
}
