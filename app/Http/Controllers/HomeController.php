<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Jenis;
use File;

class HomeController extends Controller
{
    public function index(){
        $listbuku = Buku::all();
        return view('layout.pages.halaman', compact('listbuku'));
    }
}
