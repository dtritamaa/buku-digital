<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Buku;
use App\Jenis;
use File;

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $listbuku = Buku::all();
        return view('buku.index', compact('listbuku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listjenis = Jenis::all();
        return view('buku.create', compact('listjenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'kode_buku' => 'required',
            'isi' => 'required',
            'tahun_terbit' => 'required',
            'asal_daerah' => 'required',
            'jenis_id' => 'required',
            'kategori' => 'required',
            'pengarang' => 'required',
            'cover_buku' => 'mimes:jpeg,jpg,png|max:2200',
            
        ]);
        
        $cover_buku = $request->cover_buku;
        $new_cover_buku = time() . ' - ' . $cover_buku->getClientOriginalName();
        
        $buku = Buku::create([
            'judul' => $request->judul,
            'kode_buku' => $request->kode_buku,
            'isi' => $request->isi,
            'tahun_terbit' => $request->tahun_terbit,
            'asal_daerah' => $request->asal_daerah,
            'kategori' => $request->kategori,
            'pengarang' => $request->pengarang,
            'jenis_id' => $request->jenis_id,
            'cover_buku' => $new_cover_buku, 
        ]);

        $cover_buku->move('coverbuku/', $new_cover_buku);
        Alert::success('Berhasil ! ','Berhasil menambahkan Buku !');
        return redirect('/buku');
    
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findorfail($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::findorfail($id);
        $listjenis = Jenis::all();
        return view('buku.edit', compact('listjenis', 'buku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'judul' => 'required',
            'kode_buku' => 'required',
            'isi' => 'required',
            'tahun_terbit' => 'required',
            'asal_daerah' => 'required',
            'jenis_id' => 'required',
            'kategori' => 'required',
            'pengarang' => 'required',
            'cover_buku' => 'mimes:jpeg,jpg,png|max:2200',
            
        ]);

        $buku = Buku::findorfail($id);

        if ($request->has('cover_buku')){
            $path = "coverbuku/";
            File::delete($path . $buku->cover_buku);
            $cover_buku = $request->cover_buku;
            $new_cover_buku = time() . " - " . $cover_buku->getClientOriginalName();
            $cover_buku->move("coverbuku/", $new_cover_buku);
            $post_data = [
                'judul' => $request->judul,
                'kode_buku' => $request->kode_buku,
                'isi' => $request->isi,
                'tahun_terbit' => $request->tahun_terbit,
                'asal_daerah' => $request->asal_daerah,
                'kategori' => $request->kategori,
                'pengarang' => $request->pengarang,
                'jenis_id' => $request->jenis_id,
                'cover_buku' => $new_cover_buku,
            ];
        }else{
            $post_data = [
                'judul' => $request->judul,
                'kode_buku' => $request->kode_buku,
                'isi' => $request->isi,
                'tahun_terbit' => $request->tahun_terbit,
                'asal_daerah' => $request->asal_daerah,
                'kategori' => $request->kategori,
                'pengarang' => $request->pengarang,
                'jenis_id' => $request->jenis_id,
            ];
        }
        $buku->update($post_data);
       
        Alert::success('Berhasil', 'Berhasil edit data Buku !');
        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::findorfail($id);
        $buku->delete();

        $path = "coverbuku/";
        File::delete($path . $buku->cover_buku);

        return redirect()->route('buku.index');
    }
}
