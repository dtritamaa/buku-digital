<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\Penulis;

class PenulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listpenulis = Penulis::all();
        return view('penulis.index', compact('listpenulis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penulis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|unique:penulis',
            'umur' => 'required',
            'asal_kota' => 'required',
            'biodata' => 'required'
        ]);

        Penulis::create([
            'nama' => $request -> nama,
            'umur' => $request->umur,
            'asal_kota' => $request->asal_kota,
            'biodata' => $request->biodata
        ]);

        Alert::success('Berhasil', 'Berhasil menambahkan Penulis !');
        return redirect('/penulis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penulis = Penulis::find($id);
        return view('penulis.show', compact('penulis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis = Penulis::find($id);
        return view('penulis.edit', compact('penulis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'asal_kota' => 'required',
            'biodata' => 'required',
        ]);

        $penulis = Penulis::find($id);
        $penulis -> nama = $request->nama;
        $penulis -> umur = $request->umur;
        $penulis -> asal_kota = $request->asal_kota;
        $penulis -> biodata = $request->biodata;
        $penulis -> update();

        return redirect('/penulis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penulis = Penulis::find($id);
        $penulis -> delete();
        return redirect('/penulis');
    }
}
