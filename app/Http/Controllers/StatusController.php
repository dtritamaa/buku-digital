<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Penulis;
use App\Buku;

class JenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $liststatus = Status::all();
        return view('status.index', compact('liststatus'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listbuku = Buku::all();
        $listpenulis = Penulis::all();
        return view('status.create', compact('listbuku', 'listpenulis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'buku_id' => 'required',
            'penulis_id' => 'required',
            'status' => 'required',
        ]);

        $status = Status::create([
            'buku_id' => $request -> buku_id,
            'penulis_id' => $request -> penulis_id,
            'status' => $request -> status,
        ]);

        return redirect('/status');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);
        $listbuku = Buku::all();
        $listpenulis = Penulis::all();
        return view('status.edit', compact('listbuku', 'listpenulis', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'buku_id' => 'required',
            'penulis_id' => 'required',
            'status' => 'required',
        ]);

        $status = Status::find($id);
        $status -> buku_id = $request->buku_id;
        $status -> penulis_id = $request->penulis_id;
        $status -> status = $request->status;

        $status -> update();
        
        return redirect('/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $status = Status::find($id);
        $status->delete();
        return redirect('/status');
    }
}
