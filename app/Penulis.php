<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    protected $table = ('penulis');
    protected $fillable = ['nama', 'umur', 'asal_kota', 'biodata'];

    public $timestamps = false;

    public function buku(){
        return $this->hasMany('App\Buku');
    }
}
