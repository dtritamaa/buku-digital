<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = ('jenis');
    protected $fillable = ['nama'];

    public $timestamps = false;

    public function buku(){
        return $this->hasMany('App\Buku');
    }
}
